# hpkg

hpkg is a [Rust](https://rust-lang.org) crate to manipulate Haiku's packages and repository cache files.

# Unit Testing

```
cargo test
```

# License

Copyright 2018, Alexander von Gluck IV. All rights reserved.
Released under the terms of the MIT License.

# Reference

  * [On disk format](https://git.haiku-os.org/haiku/tree/headers/private/package/hpkg/HPKGDefsPrivate.h)
