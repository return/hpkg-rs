/*
 * Copyright, 2017, Alexander von Gluck IV. All rights reserved.
 * Released under the terms of the MIT license.
 *
 * Authors:
 *   Alexander von Gluck IV <kallisti5@unixzen.com>
 */


use std::fmt;
use std::path::Path;
use std::fs::File;
use std::io;
use std::io::{Read,Seek,SeekFrom,BufReader};
use std::error;
use std::slice;


#[derive(Debug, Clone)]
#[repr(C)]
pub struct RepositoryHeader {
	pub magic: u32,
	pub header_size: u16,
	pub version: u16,
	pub total_size: u64,
	pub minor_version: u16,

	// Heap
	pub heap_compression: u16,
	pub heap_chunk_size: u32,
	pub heap_size_compressed: u64,
	pub heap_size_uncompressed: u64,

	// Repository info section
	pub info_length: u32,
	pub reserved1: u32,

	// Package attributes section
	pub package_length: u64,
	pub package_strings_length: u64,
	pub package_strings_count: u64,
}


#[derive(Debug, Clone)]
pub struct Repository {
	pub header: Option<RepositoryHeader>,

	pub name: Option<String>,
	pub url: Option<String>,
	pub base_url: Option<String>,
	pub vendor: Option<String>,
	pub summary: Option<String>,
	pub priority: Option<i32>,
	pub architecture: Option<String>,
	pub license_name: Option<String>,
	pub license_text: Option<String>,
}

fn read_struct<T, R: Read>(mut read: R) -> io::Result<T> {
	let num_bytes = ::std::mem::size_of::<T>();
	unsafe {
		let mut s = ::std::mem::zeroed();
		let buffer = slice::from_raw_parts_mut(&mut s as *mut T as *mut u8, num_bytes);
		match read.read_exact(buffer) {
			Ok(()) => Ok(s),
			Err(e) => {
				Err(e)
			}
		}
	}
}

fn parse_header<P: AsRef<Path>>(repo_file: P)
	-> Result<RepositoryHeader, Box<error::Error>> {

	let mut f = File::open(repo_file.as_ref())?;
	f.seek(SeekFrom::Start(0))?;
	let reader = BufReader::new(f);

	let header = read_struct::<RepositoryHeader, _>(reader)?;
	let magic_bytes = header.magic.to_ne_bytes();
	if magic_bytes != [b'h', b'p', b'k', b'r'] {
		return Err(From::from(format!("Unknown magic: {:?}", magic_bytes)));
	}

	Ok(header)
}

impl fmt::Display for Repository {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "repository. Name {:?}, Url(virtual) {:?}, Vendor {:?}, Summary {:?}, Arch {:?}",
			self.name, self.url, self.vendor, self.summary, self.architecture)
	}
}

impl Repository {
	pub fn new() -> Repository {
		Repository {
			header: None,
			name: None,
			url: None,
			base_url: None,
			vendor: None,
			summary: None,
			priority: None,
			architecture: None,
			license_name: None,
			license_text: None,
		}
	}

	pub fn load<P: AsRef<Path>>(repo_file: P)
		-> Result<Repository, Box<error::Error>> {

		let mut f = File::open(repo_file.as_ref())?;
		f.seek(SeekFrom::Start(0))?;

		let mut repo = Repository::new();
		repo.header = Some(self::parse_header(repo_file)?);

		return Ok(repo);
	}
}


#[test]
/// Test creating a new empty repository definition
fn test_new_repository() {
	let _repo = Repository::new();
}

#[test]
/// Test loading a valid repository from disk
fn test_load_valid_repository() {
	let repo = match Repository::load("sample/repo") {
		Ok(o) => o,
		Err(e) => {
			println!("ERROR: {}", e);
			assert!(false);
			return;
		},
	};
	assert!(repo.header.is_some());
}

#[test]
/// Test loading an invalid repository from disk
fn test_load_invalid_repository() {
    assert!(Repository::load("sample/not-repo").is_err());
}
